lucy
====

## Documentación técnica

La documentación técnica detallada y videos sobre los experimentos realizados pueden encontrarse en la [wiki del proyecto](https://gitlab.fing.edu.uy/aaguirre/lucy/wikis/home)

## Instalación

> make

## Configuración del ambiente

En el directorio 'configuration' se encuentra el archivo [SystemConf.xml](https://gitlab.fing.edu.uy/aaguirre/lucy/blob/master/configuration/SystemConf.xml) para realizar la configuración del ambiente.

Para transformar los archivos obtenidos de la base de captura de movimientos de la CMU al formato XML esperado por el sistema 
se debe de utilizar el script generateMocapXMLFiles.py, a paritr de su ejecucción los archivos de captura de movimeintos BVH, en el directorio 
especificado en el archivo de configuración del ambiente, por ejemplo: mocap/cmu_mocap/bvh/, se transorman a archivos XML en el directorio especificado en el archivo de configuración del ambiente, por ejemplo: mocap/cmu_mocap/xml/
; este directorio será tomado para poblar la población inicial del algoritmo evolutivo.

> ./generateMocapXMLFiles.py

Sabiendo que la frecuencia de muestreo utilizada en el laboratorio de la CMU al registrar los experimentos fué de 120Hz, se puede dividir dicha frecuencia utilizando la propiedad 'number of frames to skip' del archivo de configuración del ambiente, obteniendo como resultado un archivo XML el experimento generado a una frecuencia de 120/'number of frames to skip'.

Los atributos correspondientes al algoritmo evolutivo, como ser: tamaño de la población inicial, probabilidad de mutación y cruzamiento, operdores de cruzamiento, mutación y selección se encuentran en el archivo de configuración del ambiente.


## Configuración de la plataforma robótica a utilizar

En el directorio configuration se encuentra el archivo [RobotConf.xml](https://gitlab.fing.edu.uy/aaguirre/lucy/blob/master/configuration/RobotConf.xml) para realizar el mapeo entre las juntas del robot y el identificador del actuador.

## Uso

### Ejecución de los experimentos

> sudo ./lucy.sh 

En el archivo 'out.txt' se registra el log del sistema en tiempo real.

En la propiedad 'Genetic Pool' del archivo de configuración del ambiente se encuentra el nombre del directorio en el cual se registran los resultados, se genera un subdirectorio con nombre formado mediante la concatenación de la fecha y la hora actual (fecha-hora). En dicho subdirectorio se registra también un archivo info.txt con los parámetros del algoritmo genético utilizados para el experimento, por ejemplo:

```
initialPopulationSize = 32
generations = 200
genome.crossover = crossovers.G2DListCrossoverSingleNearHPoint
genome.mutator = mutators.G2DListMutatorRealGaussianSpline
MutationRate = 0.05
selector = Selectors.GTournamentSelector
CrossoverRate = 1.0
ElitismReplacement percentage = 0.3
Concatenate walk cycles = 4
concatenate external cycle file = 0
Convergence criteria enable? = 1
Vrep robot model scene = /simulator/models/genetic_bioloid_without_texture_with_ten_floor_friction.ttt
```

En el mismo subdirectorio también se almacenan los mejores individuos de cada generación y la generación final completa en el formato XML definido en el framework, 
junto con la base sqlite correspondiente a los datos de la ejecución del experimento en el archivo 'pyevolve.db', la cual es generada por la biblioteca de algoritmos genéticos utilizada y se almacenan las estádisticas de la ejecución del experimento.


### Graficos del resultado del experimento:

Puede graficarse la evolución de algoritmo genético utilizando la utilidad pyevolve_grap.py de pyevolve, debe pasarse por parámetro el nombre del experimento, 
'lucy walk' en nuestro caso, y el tipo de gráfico a realizar:

> pyevolve_grap.py -i "Lucy walk" -3

![alt text](documentation/imagenes/grafic1.png)

### Ejecución de experimentos pregrabados

Se puede ejecutar el resultado de un experimento utilizando el script testLucyEvolution.py en el directorio tests, el mismo toma por parámetros el nombre del archivo 
correspondiente al experimento; según lo especificado en el archivo de configuración de ambiente la ejecución se realizará sobre la plataforma simulada o la plataforma real.

> testLucyEvolution.py ../gene_pool/ruta_al_experimento/experimento.xml

Dependiendo del valor de la propiedad 'Lucy simulated?' la ejecución del experimento se realizará sobre la plataforma simulada (valor 1) o sobre la plataforma física (valor 0).

```xml
    <Property>
        <Name>Lucy simulated?</Name>
        <Value>1</Value>
    </Property>
```    

Existen otros parámetros relacionados con la ejecución del experimento que son también tomados del archivo [SystemConf.xml](https://gitlab.fing.edu.uy/aaguirre/lucy/blob/master/configuration/SystemConf.xml), como ser el modelo de robot a utilizar en el simulador, condición de parada, concatenaciones de movimiento, entre otros.

## License

Este proyecto está licenciado bajo GNU/GPLV2

## Autor

* **Andrés Aguirre** 

## Contribuciones

* **Gonzalo Tejera** *Guía y tutoría*
* **Javier Baliosian** *Guía y tutoría*
* **Andrés Vasilev** *Pruebas y aportes sobre aplicación en plataforma física*
* **Facundo Benavides** *Discusión y aporte de ideas en el algoritmo evolutivo*
* **Marcos Sander** *Discusión y aporte de ideas sobre el enfoque de aprendizaje por imitación*
* **Patricia Polero** *Discusión y aporte de ideas sobre biomecánica*
